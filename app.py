# -*- coding: utf-8 -*-

from flask import Flask, render_template, url_for, redirect, request, flash
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime
####################################
from flask_login import LoginManager, current_user, login_user, logout_user, login_required, confirm_login
from flask_login import UserMixin
####################################
from werkzeug.security import generate_password_hash, check_password_hash

#Import from orai.py
#from orai import *

#Requests for API URL's
import requests
import json

app = Flask(__name__)

#Secret Key
app.secret_key = 'thisismysecretkey'


#Flask login
login_manager = LoginManager(app)
#login_manager.login_view = 'app.login'
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======

#Login import Login Manager login_manager = LoginManager(app)
###from app import login_manager###

@login_manager.user_loader
def load_user(id):
    return Users.query.get(int(id))
>>>>>>> fa026f731c2a1b7112f130cd4719460fbf265a51
>>>>>>> b80373fe0422425858f14230b312531922228311

#Duomenu bazes (failo sukurimas, migracija atnaujinti duomenis)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///audi_db.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
migrate = Migrate(app, db)

#Flask login UserMixin modulis. Duomenu baze
class Users(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    vardas = db.Column(db.String(20), nullable=False)
    pastas = db.Column(db.String(100), nullable=False, unique=True)
    slaptazodis = db.Column(db.String(50), nullable=False)
    vartotojo_data = db.Column(db.DateTime, nullable=False, default=datetime.now)

    def __init__(self, vardas, pastas, slaptazodis):
        self.vardas = vardas
        self.pastas = pastas
        self.slaptazodis = slaptazodis

    def is_authenticated(self):
        return True

    def is_active(self):
        return True
    
    def is_anonymous(self):
        return False

    def __repr__(self):
        return 'Vartotojas' + str(self.id)


#Duomenu baze atsiliepimai
class Feedback(db.Model):

    #__searchable__ = ['vardas', 'pavadinimas', 'tekstas']

    id = db.Column(db.Integer, primary_key=True)
    vardas = db.Column(db.String(20), nullable=False)
    pavadinimas = db.Column(db.String(100), nullable=False, default='Atsiliepimas')
    tekstas = db.Column(db.Text, nullable=False)
    atsiliepimo_data = db.Column(db.DateTime, nullable=False, default=datetime.now)


    def __init__(self, vardas, pavadinimas, tekstas):
        self.vardas = vardas
        self.pavadinimas = pavadinimas
        self.tekstas = tekstas

    def __repr__(self):
        return 'Atsiliepimas' + str(self.id)

#Duomenu baze kontaktai - susisiekite
class Contact_Us(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    vardas = db.Column(db.String(20), nullable=False)
    telefonas = db.Column(db.Integer)
    pastas = db.Column(db.String(100), nullable=False)
    tekstas = db.Column(db.Text, nullable=False)
    komentaro_data = db.Column(db.DateTime, nullable=False, default=datetime.now)

    def __init__(self, vardas, telefonas, pastas, tekstas):
        self.vardas = vardas
        self.telefonas = telefonas
        self.pastas = pastas
        self.tekstas = tekstas

    def __repr__(self):
        return 'Komentaras' + str(self.id)

#Login import Login Manager login_manager = LoginManager(app)
###from app import login_manager###

@login_manager.user_loader
def load_user(id):
    return Users.query.get(int(id))

#Laikas
#now = datetime.now()
#laikas = now.strftime("%H:%M:%S")
#print("Laikas:", laikas)

#Pagrindinis
@app.route('/')
def index():
    return render_template('index.html')

#Automobiliai
@app.route('/visi/modeliai')
def modeliai():
    return render_template('models.html')

#Audi Servisas
@app.route('/servisas')
def servisas():
    return render_template('audi_service.html')

#Atsiliepimai
@app.route('/atsiliepimai', methods=['GET', 'POST'])
def atsiliepimai():
    if request.method == 'POST':
        ats_vardas = request.form['vardas']
        ats_pavadinimas = request.form['pavadinimas']
        ats_tekstas = request.form['tekstas']
        naujas_atsiliepimas = Feedback(vardas=ats_vardas, pavadinimas=ats_pavadinimas, tekstas=ats_tekstas)
        db.session.add(naujas_atsiliepimas)
        db.session.commit()
        return redirect('/atsiliepimai')
    else:
        visi_ats = Feedback.query.order_by(Feedback.atsiliepimo_data.desc()).all()
        return render_template('feedback.html', atsiliepimai=visi_ats)

#Atsiliepimai koreguoti
@app.route('/atsiliepimai/koreguoti/<int:id>', methods=['GET', 'POST'])
def koreguoti(id):
    atsiliepimas = Feedback.query.get_or_404(id)

    if request.method == 'POST':
        atsiliepimas.vardas = request.form['vardas']
        atsiliepimas.pavadinimas = request.form['pavadinimas']
        atsiliepimas.tekstas = request.form['tekstas']
        db.session.commit()
        return redirect('/atsiliepimai')
    else:
        return render_template('edit.html', atsiliepimas=atsiliepimas)

#Atsiliepimai istrinti
@app.route('/atsiliepimai/istrinti/<int:id>')
def istrinti(id):
    atsiliepimas = Feedback.query.get_or_404(id)
    db.session.delete(atsiliepimas)
    db.session.commit()
    return redirect('/atsiliepimai')

#Kontaktai - siusti komentara
@app.route('/kontaktai', methods=['GET', 'POST'])
def kontaktai():
    if request.method == 'POST':
        komentaro_vardas = request.form['k_vardas']
        komentaro_telefonas = request.form['k_telefonas']
        komentaro_pastas = request.form['k_pastas']
        komentaro_tekstas = request.form['k_tekstas']
        naujas_komentaras = Contact_Us(vardas=komentaro_vardas, telefonas=komentaro_telefonas, pastas=komentaro_pastas, tekstas=komentaro_tekstas)
        db.session.add(naujas_komentaras)
        db.session.commit()
        flash('Ačiū, žinutė sėkmingai išsiųsta.')
        return redirect('/kontaktai')
    else:
        #flash('Bandykite dar kartą.')
        return render_template('contacts.html')

##############
#Prisijungimas
#Formos, puslapio atvaizdavimas
@app.route('/prisijungti')
def prisijungti():
    return render_template('login.html')

#Sukuriame papildomą prisijungimo nuorodos apdorojimo versiją, kai į nuorodą /prisijungti ateinama su POST metodu perduota informacija
@app.route('/prisijungti', methods=['POST'])
def prisijungti_post():
    if request.method == 'POST':
        prisijungimo_pastas = request.form['pastas']
        prisijungimo_slaptazodis = request.form['slaptazodis']
        vartotojo_tikrinimas = Users.query.filter_by(pastas=prisijungimo_pastas).first()
         #Patikriname ar formoje pateikti duomenys sutampa su duomenimis iš db
        if not vartotojo_tikrinimas or not check_password_hash(vartotojo_tikrinimas.slaptazodis, prisijungimo_slaptazodis):
<<<<<<< HEAD
            flash("Neteisingi duomenys. Dar kartą patikrinkite prisijungimo informaciją. Arba toks vartotojas neegzistuoja.")
            return redirect('/prisijungti')
        login_user(vartotojo_tikrinimas, remember=True)
=======
<<<<<<< HEAD
            flash("Neteisingi duomenys. Dar kartą patikrinkite prisijungimo informaciją. Arba toks vartotojas neegzistuoja.")
            return redirect('/prisijungti')
        login_user(vartotojo_tikrinimas, remember=True)
=======
            flash("Neteisingi duomenys. Dar kartą patikrinkite prisijungimo informaciją.")
            return redirect('/prisijungti')
        login_user(vartotojo_tikrinimas)
>>>>>>> fa026f731c2a1b7112f130cd4719460fbf265a51
>>>>>>> b80373fe0422425858f14230b312531922228311
        return redirect('/')

##############
#Registravimas
#Formos, puslapio atvaizdavimas
@app.route('/registruotis')
def registruotis():
    return render_template('signup.html')

#Sukuriame papildomą registracijos nuorodos apdorojimo versiją, kai į nuorodą /registruotis ateinama su POST metodu perduota informacija
@app.route('/registruotis', methods=['POST'])
def registruotis_post():
    if request.method == 'POST':
        prisijungimo_vardas = request.form['vardas']
        prisijungimo_pastas = request.form['pastas']
        prisijungimo_slaptazodis = request.form['slaptazodis']
        prisijungimo_slaptazodis2 = request.form['slaptazodis2']
        vartotojo_tikrinimas = Users.query.filter_by(pastas=prisijungimo_pastas).first()

        #Jei vartotojas jau yra, peradresuojame į registracijos puslapį, be išsiųstos informacijos
        if vartotojo_tikrinimas:
            flash("Toks El. paštas jau užregistruotas.")
            return redirect('/registruotis')
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> b80373fe0422425858f14230b312531922228311
        
        #Slaptazodzio tikrinimas
        if prisijungimo_slaptazodis != prisijungimo_slaptazodis2:
            flash("Jūsų slaptažodžiai nesutampa. Prašome patikrinti informaciją.")
            return redirect('/registruotis')
        elif len(prisijungimo_slaptazodis) < 8:
            flash("Jūsų slaptažodis per trumpas. Slaptažodžio ilgis turi būti netrumpesnis nei 8 simboliai.")
            return redirect('/registruotis')

<<<<<<< HEAD
=======
=======
        elif prisijungimo_slaptazodis != prisijungimo_slaptazodis2:
            flash("Slaptažodžiai nesutampa. Prašome patikrinti ar teisingai įvedėte slaptažodžius.")
            return redirect('/registruotis')
>>>>>>> fa026f731c2a1b7112f130cd4719460fbf265a51
>>>>>>> b80373fe0422425858f14230b312531922228311

        naujas_vartotojas = Users(vardas=prisijungimo_vardas, pastas=prisijungimo_pastas, slaptazodis=generate_password_hash(prisijungimo_slaptazodis, method='sha256'))
        db.session.add(naujas_vartotojas)
        db.session.commit()
        flash("Sveikiname: Sėkmingai Užsiregistravote. Galite prisijungti.")
        return redirect('/registruotis')
    else:
        return render_template('signup.html')

#Atsijungimas
@app.route('/atsijungti')
@login_required
def atsijungti():
    logout_user()
<<<<<<< HEAD
    flash('Sėkmingai atsijungėte. Lauksime grįžtant!')
=======
<<<<<<< HEAD
    flash('Sėkmingai atsijungėte. Lauksime grįžtant!')
=======
>>>>>>> fa026f731c2a1b7112f130cd4719460fbf265a51
>>>>>>> b80373fe0422425858f14230b312531922228311
    return redirect('/')
    flash('Sėkmingai atsijungėte.')

#Orai
@app.route('/orai')
def orai():
    return render_template('weather.html')

@app.route('/orai', methods=['GET', 'POST'])
def temperatura():
    if request.method == 'POST':
        miestas = request.form['miestas']
        url = f'http://api.openweathermap.org/data/2.5/weather?q={miestas}&units=metric&appid=a11c81d7894676d0a5170ac6a1b0b15d'

        gauti_temp = requests.get(url)

        data = gauti_temp.json()
        #return data

        salis = data['sys']['country']
        temp = str(round(data['main']['temp'])) + " C"
        aprasymas = data['weather'][0]['description']
        dregme = str(data['main']['humidity']) + " %"
        vejas = str(data['wind']['speed']) + " m/s"


        return render_template('weather.html', temp=temp, dregme=dregme, vejas=vejas, aprasymas=aprasymas, miestas=miestas, salis=salis)

    return render_template('weather.html')

    

if __name__ == '__main__':
    app.run(debug=True)