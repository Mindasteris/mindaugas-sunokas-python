function slepti() {
    var x = document.getElementById('atsiliepimo_blokas');
    x.style.display = "none";
}

function rodyti() {
    var x = document.getElementById('atsiliepimo_blokas');
    x.style.display = "block";
}

(function () {

    var clockElement = document.getElementById("clock");
  
    function updateClock ( clock ) {
      clock.innerHTML = new Date().toLocaleTimeString();
    }
  
    setInterval(function () {
        updateClock( clockElement );
    }, 1000);
  
  }());


function stopVideo() {
    var video = document.getElementById("manoVideo");
    var btn = document.getElementById("manoMygtukas");

    if (video.paused) {
        video.play();
        btn.innerHTML = "Sustabdyti";
      } else {
        video.pause();
        btn.innerHTML = "Leisti";
      }
}

/*var pranesimas = document.getElementById('pranesimas');
pranesimas.style.display = "none";

function slaptazodziu_tikrinimas() {
    var x1 = document.getElementById('slaptazodis').value;
    var x2 = document.getElementById('slaptazodis2').value;
    var pranesimas = document.getElementById('pranesimas');

    if (x1 != x2 || x1 < 8) {
      //alert("Jūsų slaptažodžiai nesutampa. Prašome patikrinti informaciją.");
      pranesimas.innerHTML = "Jūsų slaptažodžiai nesutampa. Prašome patikrinti informaciją.";
    }

    pranesimas.style.display = "block";
}*/
